import os
import glob
import pandas
import xml.etree.ElementTree as et

def xmlToCSV(path):
    print('here')
    xmlList = []
    for xmlFile in glob.glob(path + '/*.xml'):
        tree = et.parse(xmlFile)
        root = tree.getroot()

        for member in root.findall('object'):
            value = (
                root.find('filename').text,
                int(root.find('size')[0].text),
                int(root.find('size')[1].text),
                member[0].text,
                int(member[4][0].text),
                int(member[4][1].text),
                int(member[4][2].text),
                int(member[4][3].text)
                )
            xmlList.append(value)
    columnName = ['filename', 'width', 'height', 'class', 'xmin', 'ymin', 'xmax', 'ymax']
    xmlDF = pandas.DataFrame(xmlList, columns=columnName)
    return xmlDF

def main():
    for folder in ['test', 'train']:
            imagePath = os.path.join(os.getcwd(), ('dataset/' + folder))
            xmlDf = xmlToCSV(imagePath)
            xmlDf.to_csv('dataset/{}_labels.csv'.format(folder), index=None)
            print('Successfully converted xml to csv.')

main()