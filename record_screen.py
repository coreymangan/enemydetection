import time
import cv2
import mss
import numpy

title = "Screen Recorder"
start_time = time.time()
frame_rate = 2
fps = 0
sct = mss.mss()
monitor = {"top": 40, "left": 0, "width": 800, "height": 600}

def screen_record():
    global fps, start_time
    while True:
        # Getting img from screen
        img = numpy.array(sct.grab(monitor))
        # Getting color
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        cv2.imshow(title, cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
        fps += 1
        TIME = time.time() - start_time

        if TIME >= frame_rate:
            print("FPS: ", fps / TIME)
            fps = 0
            start_time = time.time()
        
        # Pressing Q to quit
        if cv2.waitKey(25) & 0xFF == ord("q"):
            cv2.destroyAllWindows()
            break

screen_record()

